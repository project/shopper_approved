<?php

/**
 * @file
 * Shopper Approved Customer Ratings and Reviews.
 */

/**
 * Implements hook_menu().
 */
function shopper_approved_menu() {
  $items['admin/config/shopperapproved'] = array(
    'title' => 'Shopper Approved Setting Page',
    'page callback' => 'drupal_get_form',
    'access callback' => 'shopper_approved_user_has_role',
    'page arguments' => array('shopper_approved_form'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Menu callback function.
 */
function shopper_approved_user_has_role() {
  global $user;
  if (in_array("administrator", $user->roles)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Shopper approved required field form.
 */
function shopper_approved_form($form, &$form_state) {
  $form['sa_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Shopper Approved ID',
    '#required' => TRUE,
    '#default_value' => variable_get('shopper_approved_id', ''),
  );

  $form['sa_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Shopper Approved Token',
    '#required' => TRUE,
    '#default_value' => variable_get('shopper_approved_token', ''),
  );

  $form['sa_site'] = array(
    '#type' => 'textfield',
    '#title' => 'Shopper Approved Website',
    '#required' => TRUE,
    '#default_value' => variable_get('shopper_approved_site', ''),
  );

  // Get the all content type.
  $types = node_type_get_types();
  $content_type = array('' => '--Select--');

  foreach ($types as $values) {
    $content_type[$values->type] = $values->name;
  }

  $form['content_type'] = array(
    '#type' => 'select',
    '#title' => 'Display content type',
    '#options' => $content_type,
    '#required' => TRUE,
    '#default_value' => variable_get('shopper_approved_content_type', ''),
    '#prefix' => '',
  );

  $form['product_id_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Product Id Field (engine name)',
    '#required' => TRUE,
    '#default_value' => variable_get('shopper_approved_product_id_field', ''),
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Shopper approved submit function.
 */
function shopper_approved_form_submit($form, &$form_state) {
  $sa_values = $form_state['values'];

  $sa_id = $sa_values['sa_id'];
  $sa_site = $sa_values['sa_site'];
  $sa_token = $sa_values['sa_token'];
  $content_type = $sa_values['content_type'];
  $product_id_field = $sa_values['product_id_field'];

  variable_set('shopper_approved_id', $sa_id);
  variable_set('shopper_approved_site', $sa_site);
  variable_set('shopper_approved_token', $sa_token);
  variable_set('shopper_approved_content_type', $content_type);
  variable_set('shopper_approved_product_id_field', $product_id_field);
}

/**
 * Implements hook_field_extra_fields().
 */
function shopper_approved_field_extra_fields() {

  $content_type = variable_get('content_type', '');
  $extra['node'][$content_type] = array(
    'display' => array(
      'shopper_approved_ratings' => array(
        'label' => t('Shopper Approved'),
        'description' => t('Shopper Approved Customer Ratings.'),
        'weight' => 99,
      ),
      'shopper_approved_reviews' => array(
        'label' => t('Shopper Review'),
        'description' => t('Shopper Customer Reviews.'),
        'weight' => 100,
        'visible' => FALSE,
      ),
    ),
  );
  return $extra;
}

/**
 * Implements hook_node_view().
 */
function shopper_approved_node_view($node, $view_mode, $langcode) {
  $extra = shopper_approved_field_extra_fields();
  $sa_id = variable_get('shopper_approved_id', '');
  $sa_site = variable_get('shopper_approved_site', '');
  $product_id_value = variable_get('shopper_approved_product_id_field', '');

  // Check that we're supporting the node type being viewed.
  if (empty($extra['node'][$node->type]['display'])) {
    return;
  }
  $product_id = $node->{$product_id_value};

  $config = field_bundle_settings('node', $node->type);
  foreach ($extra['node'][$node->type]['display'] as $field_name => $field_info) {
    // Check to make sure this field is visible in this view mode.
    if (empty($config['extra_fields']['display'][$field_name][$view_mode]['visible'])) {
      continue;
    }
    $script = "";
    if ($field_name == 'shopper_approved_ratings') {
      $script = '<script type="text/javascript">';
      $script .= 'var sa_products_count = 3;';
      $script .= 'var sa_date_format = "F j, Y"; ';
      $script .= 'var sa_product =  ' . $product_id . ' ;';
      $script .= 'function saLoadScript(src) {
      var js = window.document.createElement("script"); js.src = src; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js);
} ';
      $script .= 'saLoadScript("//www.shopperapproved.com/product/' . $sa_id . '/"+sa_product+".js");';
      $script .= '</script>';
      $shopper = '<div id="review_header"></div><div id="product_page"></div>';
      $shopper .= '<div id="review_image">';
      $shopper .= '<a href="http://www.shopperapproved.com/reviews/' . $sa_site . '/"
  onclick="var nonwin=navigator.appName!=\'Microsoft Internet Explorer\'?\'yes\':\'no\';';
      $shopper .= 'var certheight=screen.availHeight-90; window.open(this.href,\'shopperapproved\',\'location=\'+nonwin+\',scrollbars=yes,width=620,height=\'+certheight+\',menubar=no,toolbar=no\'); return false;" target="_blank" rel="nofollow"></a></div>';
      $node->content[$field_name] = array('#markup' => $script . $shopper);
    }
    elseif ($field_name == 'shopper_approved_reviews') {
      $script = '<script type="text/javascript">';
      $script .= 'var sa_product = ' . $product_id . ';';
      $script .= 'function saLoadScript(src) {
    var js = window.document.createElement("script");
    js.src = src;
    js.type = "text/javascript";
    document.getElementsByTagName("head")[0].appendChild(js);
    } ';

      $script .= 'saLoadScript("//www.shopperapproved.com/product/' . $sa_id . '/"+sa_product+".js"); ';
      $script .= '</script>';
      $reviews = '<div id="product_just_stars" class="reg"></div>';
      $node->content[$field_name] = array('#markup' => $script . $reviews);
    }
  }
}

/**
 * Hook_checkout_complete_checkout_form.
 */
function shopper_approved_checkout_complete_checkout_form($form, &$form_state, $checkout_pane, $order) {
  global $user;

  $order_number = $order->order_number;
  $name = $user->name;
  $email = $user->mail;
  $sa_id = variable_get('shopper_approved_id', '');
  $sa_token = variable_get('shopper_approved_token', '');

  $script = '<script type="text/javascript">var sa_values = { "site":"' . $sa_id . '", "orderid":"' . $order_number . '","name":"' . $name . '","email":"' . $email . '", "forcecomments":1, "emailAll":1,"token":"' . $sa_token . '" };function saLoadScript(src) { var js = window.document.createElement("script");js.src = src;js.type = "text/javascript";document.getElementsByTagName("head")[0].appendChild(js); } var d = new Date(); if (d.getTime() - 172800000 > 1461333211000)saLoadScript("//www.shopperapproved.com/thankyou/rate/' . $sa_id . '.js"); else saLoadScript("//direct.shopperapproved.com/thankyou/rate/' . $sa_id . '.js?d=" + d.getTime()); </script>';
  print $script;
}
